import gsap from 'gsap/all';
import Monster from './Monster';

export default class Animation {

    constructor() {

        this.monsters = [];

        let monst = document.getElementsByClassName("monster");

        for(let i = 0; i < monst.length; i++) {

            let newMonster = new Monster(i);

            monst[i].addEventListener('mouseenter', fun => {
                newMonster.contract();
                newMonster.expand();
            })

            monst[i].addEventListener('mouseleave', fun => {
                newMonster.reset();
            })

            this.monsters.push(newMonster);
        }
    }
}