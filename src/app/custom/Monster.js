import gsap from 'gsap/all';

export default class Monster {

    constructor(name) {
        this.id = name;
    }

    expand() {
        gsap.to('#miro' + this.id, {id: "expand", duration: 0.5, width: "80%"});
    }

    contract() {
        gsap.to('.monster', {id: "contract", duration: 0.5, width: "4%"});
    }

    reset() {
        gsap.to('.monster', {id: "reset", duration: 0.5, width: "16.6%"});
    }
}